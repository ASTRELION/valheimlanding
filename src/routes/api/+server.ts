import { env } from "$env/dynamic/private";
import { json } from "@sveltejs/kit";
import GameDig from "gamedig";

export type ValheimData = GameDig.QueryResult &
{
    version: string,
    success: boolean,
    timestamp: number
}

const STALE_TIME = 15000;
let lookupData: ValheimData;
let lastLookupData: ValheimData;

export async function GET()
{
    const now = new Date().getTime();
    if (lookupData && lookupData.timestamp + STALE_TIME > now)
    {
        return json(lookupData);
    }

    try
    {
        const response = await GameDig.query({
            type: "valheim",
            host: String(env.SERVER_QUERY_URL),
            port: Number(env.SERVER_QUERY_PORT),
            requestRules: true
        });
        lookupData = response as ValheimData;
        if (response.raw)
        {
            const key = "tags" as keyof typeof response.raw;
            const tags: string[] = response.raw[key];
            lookupData = {
                ...lookupData,
                version: tags[0].split("g=")[1],
            } as ValheimData;
        }
        lookupData = {
            ...lookupData,
            success: true,
            timestamp: now
        };
        lastLookupData = lookupData;
    }
    catch
    {
        const lookup = {
            success: false,
            timestamp: now
        } as ValheimData;

        if (lastLookupData && lastLookupData.success)
        {
            lookupData = {
                ...lastLookupData,
                timestamp: now
            };
        }
        else
        {
            lookupData = lookup;
        }

        lastLookupData = lookup;
    }

    return json(lookupData);
}
