# https://kit.svelte.dev/docs/adapter-node#deploying

FROM node:18 as build

WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:18-alpine as production

WORKDIR /app
COPY --from=build /app/package*.json .
COPY --from=build /app/build build
RUN npm ci --omit dev

ENV NODE_ENV=production
ENV HOST=0.0.0.0
ENV PORT=3000

EXPOSE ${PORT}

# https://kit.svelte.dev/docs/adapter-node#environment-variables
CMD [ "node", "-r", "dotenv/config", "build" ]
