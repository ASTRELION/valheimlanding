# ValheimLanding

Simple [SvelteKit](https://kit.svelte.dev/)-based landing for my [Valheim](https://www.valheimgame.com/) server.

## Usage

### Docker

Copy the [`docker-compose.yml`](./docker-compose.yml) file, or use `docker run`:
```bash
docker run registry.gitlab.com/astrelion/valheimlanding:latest \
-p 3000:3000 \
-e PUBLIC_SERVER_URL="valheim.example.com" \
-e PUBLIC_SERVER_PORT="2456" \
-e SERVER_QUERY_URL="valheim.example.com" \
-e SERVER_QUERY_PORT="2457" \
-e PUBLIC_SOURCE_URL="https://gitlab.com/ASTRELION/valheim-landing" \
-e PUBLIC_DONATE_URL="https://liberapay.com/ASTRELION/"
```

*All env variables are required.*

## Build

### Node

*Create the `.env` file with the four variables required (defined above)*.

#### Production

1. `npm install`
2. `npm run build`
3. `node -r dotenv/config build`

#### Dev

1. `npm install`
2. `npm run dev`

### Docker

1. `docker build -t valheimlanding .`
2. `docker run valheimlanding [...]` *See Usage > Docker for full command*
